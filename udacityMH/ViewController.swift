//
//  ViewController.swift
//  udacityMH
//
//  Created by medright on 11/7/17.
//  Copyright © 2017 evermedresearch. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

//bounding box
extension SCNNode {
    func boundingBoxContains(point: SCNVector3, in node: SCNNode) -> Bool {
        let localPoint = self.convertPosition(point, from: node)
        return boundingBoxContains(point: localPoint)
    }
    
    func boundingBoxContains(point: SCNVector3) -> Bool {
        return BoundingBox(self.boundingBox).contains(point)
    }
}

struct BoundingBox {
    let min: SCNVector3
    let max: SCNVector3
    
    init(_ boundTuple: (min: SCNVector3, max: SCNVector3)) {
        min = boundTuple.min
        max = boundTuple.max
    }
    
    func contains(_ point: SCNVector3) -> Bool {
        let contains =
            min.x <= point.x &&
                min.y <= point.y &&
                min.z <= point.z &&
                
                max.x > point.x &&
                max.y > point.y &&
                max.z > point.z
        
        return contains
    }
}



class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var hatNode: SCNNode?
    private var planeAnchor: ARPlaneAnchor?
    
    // thrown objects array
    private var cubeNodes = [SCNNode]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        self.sceneView.debugOptions  = [.showConstraints, ARSCNDebugOptions.showFeaturePoints]
        
        // Set the scene to the view
        sceneView.scene = SCNScene()
        
        addButton()
        addButton2()
    }
    
    //creates a ball geometry for throwing when CubeButton is tapped from the direction of the camera
    @IBAction func addCubeButtonTapped(sender: UIButton) {
        DispatchQueue.main.async {
            print("cube button tapped")
            let currentFrame = self.sceneView.session.currentFrame
            let cubeNode = SCNNode(geometry: SCNSphere(radius: 0.07))
            let force = simd_make_float4(-1, 1, -5, 0)
            let rotatedForce = simd_mul((currentFrame?.camera.transform)!, force)
            let vectorForce = SCNVector3(x:rotatedForce.x, y:rotatedForce.y, z:rotatedForce.z)
            cubeNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
            cubeNode.physicsBody?.applyForce(vectorForce, asImpulse: true)
            cubeNode.physicsBody?.friction = 1.0
            
            // Add current cubeNode node to cubeNodes array
            self.cubeNodes.append(cubeNode)
            self.sceneView.pointOfView?.addChildNode(cubeNode)
        }
    }
    
    func addButton() {
        let button = UIButton()
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Toss a ball", for: .normal)
        button.setTitleColor(UIColor.red, for: .normal)
        button.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        button.addTarget(self, action: #selector(addCubeButtonTapped(sender:)) , for: .touchUpInside)
        
        // Contraints
        button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8.0).isActive = true
        button.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50)
    }
    
    //button for hiding ball thrown into the hat
    
    @IBAction func hideCubeButtonTapped(sender: UIButton) {
        guard let hatNode = hatNode?.presentation else { return }
        
        print("Hide Button Tapped")
        for cubeNode in cubeNodes {
            
            // hide/show cubeNodes in the hat
            if (hatNode.boundingBoxContains(point: cubeNode.presentation.position)) {
                if cubeNode.isHidden == true {
                   cubeNode.isHidden = false
                } else {
                    cubeNode.isHidden = true
                }
            }
        }
    }
    
    func addButton2() {
        let button = UIButton()
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Hide balls", for: .normal)
        button.setTitleColor(UIColor.red, for: .normal)
        button.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        button.addTarget(self, action: #selector(hideCubeButtonTapped(sender:)) , for: .touchUpInside)
        
        // Contraints
        button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8.0).isActive = true
        button.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
    
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        //add hat scene if no scene is placed on detected plane
        guard let planeAnchor = anchor as? ARPlaneAnchor, hatNode == nil else { return nil }
        
        self.planeAnchor = planeAnchor
        
            let mhatScene = SCNScene(named: "art.scnassets/hat.scn")!
            hatNode = mhatScene.rootNode.childNode(withName: "tube", recursively: true)!
            hatNode?.position = SCNVector3(anchor.transform.columns.3.x, anchor.transform.columns.3.y, anchor.transform.columns.3.z)
            
              return hatNode
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.center == self.planeAnchor?.center || self.planeAnchor == nil else { return }
        
        // Set the floor's geometry to be the detected plane
        let floor = sceneView.scene.rootNode.childNode(withName: "floor", recursively: true)
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.y))
        floor?.geometry = plane
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
